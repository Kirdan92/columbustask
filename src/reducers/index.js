import { ADD_ITEM_TO_CART } from "../constants/action-types";
import { UPDATE_CART_ITEM } from "../constants/action-types";

import { CLEAR_CART } from "../constants/action-types";
import { GET_PRODUCTS } from "../constants/action-types";
import { LOADING_DATA } from "../constants/action-types";

let initialState = {
  product: [],
  categories: [
    "Paper clips",
    "Post-it notes",
    "Staples",
    "Hole punches",
    "Binders",
    "Staplers",
    "Laminators",
    "Writing utensils",
    "Paper",
    "Computers",
    "Printers",
    "Fax machines",
    "Photocopiers",
    "Cash registers",
    "Office furniture",
    "Chairs",
    "Cubicles",
    "Filing cabinet",
    "Armoire desks",
  ],
  loadingData: false,
  cart:
    "localStorage" in window && "storageCartObject" in window.localStorage
      ? JSON.parse(window.localStorage["storageCartObject"])
      : {
          active: false,
          producsList: [],
        },
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      return { ...state, product: action.products, loadingData: false };

    case LOADING_DATA:
      return { ...state, loadingData: action.actionValue };

    case CLEAR_CART:
      if ("localStorage" in window) {
        window.localStorage["storageCartObject"] = JSON.stringify({
          producsList: [],
        });
      }
      return {
        ...state,
        cart: {
          producsList: [],
        },
      };

    case ADD_ITEM_TO_CART:
      if ("localStorage" in window) {
        window.localStorage["storageCartObject"] = JSON.stringify({
          producsList: [...state.cart.producsList, action.product],
        });
      }

      return {
        ...state,
        cart: {
          ...state.cart,
          producsList: [...state.cart.producsList, action.product],
        },
      };
    case UPDATE_CART_ITEM:
      if (action.effect === "add") {
        const list = state.cart.producsList.map((product) => {
          return product.name === action.name
            ? { ...product, quantity: product.quantity + 1 }
            : { ...product, quantity: product.quantity };
        });

        if ("localStorage" in window) {
          window.localStorage["storageCartObject"] = JSON.stringify({
            producsList: [...list],
          });
        }
        return {
          ...state,
          cart: { ...state.cart, producsList: [...list] },
        };
      }
      if (action.effect === "substract") {
        const list = state.cart.producsList.map((product) => {
          return product.name === action.name
            ? product.quantity == 1
              ? null
              : { ...product, quantity: product.quantity - 1 }
            : { ...product, quantity: product.quantity };
        });
        if ("localStorage" in window) {
          window.localStorage["storageCartObject"] = JSON.stringify({
            producsList: [...list],
          });
        }
        return {
          ...state,
          cart: { ...state.cart, producsList: [...list] },
        };
      }
      if (action.effect === "remove") {
        const list = state.cart.producsList.filter(
          (product) => product.name !== action.name
        );

        if ("localStorage" in window) {
          window.localStorage["storageCartObject"] = JSON.stringify({
            producsList: [...list],
          });
        }
        return {
          ...state,
          cart: { ...state.cart, producsList: [...list] },
        };
      }
    default: {
      return {
        ...state,
      };
    }
  }
};

export default rootReducer;
