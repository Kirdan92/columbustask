import React, { Component } from "react";
import store from "./store/index";
import { Provider } from "react-redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";

import Home from "./components/Home";

import { Helmet } from "react-helmet";

import "./assets/scss/style.scss";

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
   
    return (
      <React.Fragment>
        <Helmet>
          <title>Technical Task - Front End</title>
          <meta name="keywords" content="technical task front edn" />
          <meta name="description" content="Technical Task - Front End." />
        </Helmet>
        <Header />

        <Switch>
          <Route exact path="/" component={() => <Home />} />
        </Switch>
        <Footer />
      </React.Fragment>
    );
  }
}



export default (App);
