import React, { PureComponent } from "react";

import TopHeader from "./TopHeader";
import BottomHeader from "./BottomHeader";
import "./Header.scss";

class Header extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="section-wrapper">
        <TopHeader />
        <BottomHeader />
      </div>
    );
  }
}

export default Header;
