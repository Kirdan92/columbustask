import React, { PureComponent } from "react";

import "./CartHeader.scss";

class CartHeader extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  getProductAmount = () => {
    const productListQuantity = this.props.cartData.producsList.reduce(
      function (total, currentValue) {
        return total + currentValue.quantity;
      },
      0
    );

    return productListQuantity;
  };

  getProductTotalPrice = () => {
    const productListPrice = this.props.cartData.producsList.reduce(function (
      total,
      currentValue
    ) {
      return total + currentValue.product.price * currentValue.quantity;
    },
    0);
    console.log("productListPrice", productListPrice);
    return productListPrice;
  };

  clearCart = () => {
    this.props.clearCart();
  };
  render() {
    const { cartData } = this.props;
    return (
      <div className="cart-header__container">
        <div className="cart-icon__container">
          <i className="las la-shopping-cart"></i>
          {this.props.cartData.producsList.length > 0 ? (
            <div className="cart-icon-productList">
              <p className="cart-icon-productList-number">
                {cartData &&
                "producsList" in cartData &&
                cartData.producsList.length
                  ? `${this.getProductAmount()} `
                  : null}
              </p>
            </div>
          ) : null}
        </div>
        <div className="product-amount__container">
          {" "}
          {cartData && "producsList" in cartData && cartData.producsList.length
            ? `${this.getProductAmount()} st`
            : null}
        </div>
        <div className="product-price__container">
          {cartData && "producsList" in cartData && cartData.producsList.length
            ? `${this.getProductTotalPrice()} kr`
            : null}
        </div>
        <button className="checkout-btn main-btn" onClick={this.clearCart}>
          Check out
        </button>
      </div>
    );
  }
}

export default CartHeader;
