import { connect } from "react-redux";

import CartHeader from "./CartHeader";

import { clearCart } from "../../../../actions/index";

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    clearCart: () => dispatch(clearCart()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartHeader);
