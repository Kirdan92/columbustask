import React, { PureComponent } from "react";

import "./SearchForm.scss";

class SearchForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      searchField: "",
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
  };

  searchFieldHandler = (event) => {
    this.setState({ searchField: event.target.value });
  };

  handleKeyDown(e) {
    if (e.key === "Enter") {
      this.setState({ searchField: "" });
    }
  }

  render() {
    return (
      <div className="search-form__container">
        <form  onSubmit={this.handleSubmit}>
          <div className="input-field">
            <button
              className="btn-search"
              type="button"
              onClick={this.handleClick}
            >
              <i className="las la-search"></i>
            </button>
            <input
              id="search"
              className="input-search"
              type="text"
              placeholder="Search for products ..."
              value={this.state.searchField}
              onChange={this.searchFieldHandler}
              onKeyPress={(e) => this.handleKeyDown(e)}
            />
          </div>
          <button
            className="confirm-search__btn"
            type="button"
            onClick={this.handleClick}
          >
            Search
          </button>
        </form>
      </div>
    );
  }
}

export default SearchForm;
