import React, { PureComponent } from "react";
import SearchForm from "./SearchForm";
import CartHeader from "./CartHeader";

import "./TopHeader.scss";
import logo from "../../../assets/images/OfficeSupplies.png";
class TopHeader extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="top-header__wrapper">
        <div className="hambureger">
        <i className="las la-bars"></i>
        </div>
        <div className="logo-container">
          <img src={logo} alt="Company logo" />
        </div>
        <SearchForm />
        <CartHeader cartData={this.props.cart} />
      </div>
    );
  }
}

export default TopHeader;
