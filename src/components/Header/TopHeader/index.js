import { connect } from "react-redux";

import TopHeader from "./TopHeader";

const mapStateToProps = (state) => {
  return { cart: state.cart };
};

export default connect(mapStateToProps)(TopHeader);
