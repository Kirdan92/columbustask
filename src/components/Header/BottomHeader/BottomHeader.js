import React, { PureComponent } from "react";
import { NavLink, Link } from "react-router-dom";
import "./BottomHeader.scss";

class BottomHeader extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="bottom-header__wrapper">
        <Link className="link-item link-text " to="/">All of our products</Link>
        <Link className="link-item link-text " to="/">Inspiration for the office </Link>
        <Link className="link-item link-text " to="/">About OS </Link>
      </div>
    );
  }
}

export default BottomHeader;
