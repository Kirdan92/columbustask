import React, { PureComponent } from "react";
import CategoryItem from "./CategoryItem";

import "./Categories.scss";

class Categories extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
   
    return (
      
      <div className="categories-wrapper">
        {this.props.categories.map((catItem, i) => (
          <CategoryItem key={`ci${i}`} name={catItem} />
        ))}
      </div>
    );
  }
}

export default Categories;
