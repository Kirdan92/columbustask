import React from "react";
import { Link } from "react-router-dom";
import "./CategoryItem.scss";

const CategoryItem = (props) => (
  <Link to="#" className="category-item link-text" >
    {props.name}
  </Link>
);

export default CategoryItem;
