import { connect } from "react-redux";
import PropTypes from "prop-types";
import Home from "./Home";
import { getProducts } from "../../actions/index";
import { setLoadingData } from "../../actions/index";

// Home.propTypes = {
//   getProducts: PropTypes.func.isRequired,
//   products: PropTypes.objext.isRequired,
// };

const mapStateToProps = (state) => {
  return { product: state.product };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProducts: (products) => dispatch(getProducts(products)),
    setLoadingData: () => dispatch(setLoadingData),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
