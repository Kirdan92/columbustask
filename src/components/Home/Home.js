import React, { PureComponent } from "react";
import Categories from "../Categories";
import ProductCard from "../ProductCard";

import "./Home.scss";

class Home extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      products: [],
    };
  }
  componentDidMount() {
    if (!this.state.products.length) {
      this.getData();
    }
  }

  getData = () => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/product/get-products", true);

    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify());
    xhr.onload = () => {
      let data = JSON.parse(xhr.responseText);
      if ("ok" in data && data.ok) {
        this.setState({
          products: data.products,
          isLoading: false,
        });
        this.props.getProducts(data.products);
      }
    };
  };

  render() {

    if (this.state.isLoading) {
      return <div className=" home-section"></div>;
    } else {
      return (
        <div className=" home-section">
          <Categories />
          <ProductCard productsData={this.state.products} />
        </div>
      );
    }
  }
}

export default Home;
