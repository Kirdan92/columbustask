import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/images/OfficeSupplies.png";
import "./Footer.scss";

class Footer extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="footer-wrapper ">
        <div className="footer-left__container">
          <div className="logo-container">
            <img src={logo} alt="Company logo" />
          </div>
          <div className="footer-left__text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
            euismod bibendum laoreet. Proin gravida dolor sit amet lacus
            accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
            Cum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
            vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget
            odio.
          </div>
        </div>
        <div className="footer-right__container">
          <div className="bestselelrs__container">
            <h4 className="footer-title">Bestsellers</h4>
            <ul className="bestsellers-list">
              <Link className="link-text" to="/">
                {" "}
                <li>A4</li>
              </Link>
              <Link className="link-text" to="/">
                {" "}
                <li>A5</li>
              </Link>
              <Link className="link-text" to="/">
                {" "}
                <li>Pens</li>
              </Link>
              <Link className="link-text" to="/">
                {" "}
                <li>Staplers</li>
              </Link>
            </ul>
          </div>
          <div className="social-links__container">
            <h4 className="footer-title">We're social!</h4>
            <ul className="social-list">
              <Link className="link-text" to="/">
                {" "}
                <li>
                  <i className="lab la-facebook"></i>Facebook
                </li>
              </Link>
              <Link className="link-text" to="/">
                {" "}
                <li>
                  <i className="lab la-twitter"></i>Twitter
                </li>
              </Link>
              <Link className="link-text" to="/">
                {" "}
                <li>
                  <i className="lab la-linkedin"></i>Linked in
                </li>
              </Link>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
