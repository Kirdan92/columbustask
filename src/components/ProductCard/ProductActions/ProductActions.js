import React, { PureComponent } from "react";

import "./ProductActions.scss";

class ProductActions extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedColor: 0,
    };

    this.product = false;
  }

  componentDidMount() {
    this.product = this.props.productData;
  }
  addToCart = () => {
    const producsList = this.props.cart.producsList;

    const isInCart = producsList.find(
      (product) => product.name == this.product.name
    );

    if (isInCart) {
      this.props.updateCartItem(this.product.name, "add");
    } else {
      this.props.addItemToCart({
        name: this.product.name,
        product: this.product,
        quantity: 1,
      });
    }
  };
  handleColorChage = (event) => {
    this.setState({ selectedColor: event.target.value });
  };
  render() {

    const producsList = this.props.cart.producsList;
 
    const { productData } = this.props;
    return (
      <div className="product-actions__wrapper">
        <div className="product-actions__left">
          <div className="prices__container">
            <h3 className="product_price">{`${String(
              Number(productData.price).toFixed(2)
            ).replace(".", ",")} kr`}</h3>
            <h6 className="product-old__price">{`${productData.oldPrice} kr`}</h6>
          </div>
          <div className="color-select">
            <select 
              value={this.state.selectedColor}
              onChange={this.handleColorChage}
            >
              {this.props.productData && "colors" in this.props.productData
                ? this.props.productData.colors.map((color, index) => (
                    <option key={`co${index}`} value={index}>{color}</option>
                  ))
                : null}
            </select>
             <div className="select__arrow"></div>
          </div>
        </div>
        <div className="product-actions__right">
          <button className="main-btn" onClick={this.addToCart}>
            Buy
          </button>
        </div>
      </div>
    );
  }
}

export default ProductActions;
