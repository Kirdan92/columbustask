import { connect } from "react-redux";

import ProductActions from "./ProductActions";

import { addItemToCart } from "../../../actions/index";
import { updateCartItem } from "../../../actions/index";

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addItemToCart: (product) => dispatch(addItemToCart(product)),

    updateCartItem: (name, action) => dispatch(updateCartItem(name, action)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductActions);
