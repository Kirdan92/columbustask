import React, { PureComponent } from "react";

import "./ProductDescription.scss";

class ProductDescription extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      folded: true,
    };
  }

  unfoldText = () => {
    this.setState({
      folded: ! this.state.folded,
    });
  };
  render() {
    const { productData } = this.props;
    const { folded } = this.state;
    return (
      <div className="product-description__wrapper">
        <div className="description-text">
          {folded
            ? `${String(productData.descritpion).slice(0, 500)}...`
            : productData.descritpion}
        </div>
        <button onClick={this.unfoldText} className="read-more__btn main-btn">Read more</button>
      </div>
    );
  }
}

export default ProductDescription;
