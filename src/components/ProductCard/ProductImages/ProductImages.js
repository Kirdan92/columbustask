import React, { PureComponent } from "react";

import "./ProductImages.scss";

class ProductImages extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedImage: 0,
    };
  }

  selectImage = (index) => {
    this.setState({
      selectedImage: index,
    });

    let bigImage = document.querySelector("#big-image");
    bigImage.classList.remove("fade-in");
     bigImage.classList.add("invisible");
    setTimeout(() => {
      bigImage.classList.add("fade-in");
    }, 300);
  };

  fadeIn = (img) => {
    img.target.classList.add("fade-in");
  };
  render() {
    
    const { productData } = this.props;
    const { selectedImage } = this.state;
    return (
      <div className="product-images__wrapper">
        <div className="big-image__box">
          <img
            src={require(`../../../assets/images/productImages/${productData.imagesArr[selectedImage]}`)}
            alt="Big pictrue of product"
            id="big-image"
          />
        </div>
        <div className="image-miniatures__box">
          {"imagesArr" in productData && productData.imagesArr.length
            ? productData.imagesArr.map((image, index) => {
                return (
                  <div
                  key={`pi${index}`}
                    className={
                      "image-miniature__item " +
                      (index == selectedImage ? "selected" : "")
                    }
                    onClick={() => this.selectImage(index)}
                  >
                    <img
                      src={require(`../../../assets/images/productImages/${image}`)}
                      alt="Big pictrue of product"
                    />
                  </div>
                );
              })
            : null}
        </div>
      </div>
    );
  }
}

export default ProductImages;
