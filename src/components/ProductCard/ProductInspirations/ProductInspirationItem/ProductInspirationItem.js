import React from "react";

import "./ProductInspirationItem.scss";

const ProductInspirationItem = (props) => (
  <div className="product-inspiration__container">
    <h4>{props.data.title}</h4>
    <p>{props.data.text}</p>
  </div>
);

export default ProductInspirationItem;
