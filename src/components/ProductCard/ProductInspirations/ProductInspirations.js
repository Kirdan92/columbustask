import React, { PureComponent } from "react";
import ProductInspirationItem from "./ProductInspirationItem";
import "./ProductInspirations.scss";

const tempData = [
  {
    title: "Paper is awesome!",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.",
  },
  {
    title: "Paper is awesome!",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.",
  },
  {
    title: "Paper is awesome!",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet la.",
  },
];

class ProductInspirations extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { productData } = this.props;
    return (
      <div className="product-inspirations__wrapper">
        {tempData.map((item, index) => (
          <ProductInspirationItem key={`pi${index}`} data={item} />
        ))}
      </div>
    );
  }
}

export default ProductInspirations;
