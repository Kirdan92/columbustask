import React, { PureComponent } from "react";
import ProductImages from "./ProductImages";
import ProductActions from "./ProductActions";
import ProductDescription from "./ProductDescription";
import ProductInspirations from "./ProductInspirations";

import "./ProductCard.scss";

class ProductCard extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { productsData } = this.props;
    return (
      <div className="product-card__wrapper">
        <ProductImages productData={productsData[0]} />
        <h2 className="product-title">{productsData[0].name}</h2>
        <ProductActions productData={productsData[0]} />
        <ProductDescription productData={productsData[0]} />
        <ProductInspirations />
      </div>
    );
  }
}

export default ProductCard;
