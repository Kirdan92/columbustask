import axios from "axios";
import { GET_PRODUCTS } from "../constants/action-types";
import { LOADING_DATA } from "../constants/action-types";

//Cart actions
import { ADD_ITEM_TO_CART } from "../constants/action-types";
import { UPDATE_CART_ITEM } from "../constants/action-types";
import { CLEAR_CART } from "../constants/action-types";

export const getProducts = (products) => ({
  type: GET_PRODUCTS,
  products,
});

export const setLoadingData = () => ({ type: LOADING_DATA });

export const addItemToCart = (product) => ({ type: ADD_ITEM_TO_CART, product });
export const clearCart = () => ({
  type: CLEAR_CART,
});

export const updateCartItem = (name, effect) => ({
  type: UPDATE_CART_ITEM,
  name,
  effect,
});
