export const ADD_ITEM_TO_CART = "ADD_ITEM_TO_CART";
export const UPDATE_CART_ITEM = "UPDATE_CART_ITEM";
export const SHOW_CART = "SHOW_CART";
export const CHANGE_CART = "CHANGE_CART";
export const CLEAR_CART = "CLEAR_CART";
export const GET_PRODUCTS = "GET_PRODUCTS";
export const LOADING_DATA = "LOADING_DATA";
