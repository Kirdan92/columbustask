var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
  name: { type: String },
  descritpion: { type: String },
  price: { type: Number },
  oldPrice: { type: Number },
  imagesArr: [{ type: String }],
  colors: [{ type: String }],
  dateofCreate: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Product", ProductSchema);
