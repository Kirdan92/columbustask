const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const products = require("./api/product");
const { mongodbUrl } = require("../../config/db");

const app = express();

app.use(bodyParser.json());

mongoose
  .connect(mongodbUrl)
  .then(() => console.log("Mongoose connected"))
  .catch((err) => console.log(err));

//Routes
app.use("/api/product", products);

const port = process.env.PORT || 8001;

app.listen(port, () => console.log("Listening on port ", port));
