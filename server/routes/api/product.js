const express = require("express");
const router = express.Router();


const Product = require("../../models/product");

router.get("/get-products", (req, res) => {
  Product.find().then((products) => res.json({ ok: true, products: products }));
});

router.post("/add", (req, res) => {
  let newProduct = new Product({
    name: req.body.name,
    descritpion: req.body.descritpion,
    price: req.body.price,
    oldPrice: req.body.oldPrice,
    imagesArr: req.body.imagesArr,
    colors: req.body.colors,
  });
  newProduct.save().then((product) => res.json(product));
});

module.exports = router;
